import java.io.IOException;
import java.lang.Comparable;
public class TimePiece {
	
	    private Tiempo inicio;
	    private Tiempo fin;

	    public TimePiece(Tiempo start, Tiempo last){
	        this.inicio = start;
	        this.fin = last;
	    }
	    public Tiempo getinicio(){
	        return this.inicio;
	    }
	    public Tiempo getfin(){
	        return this.fin;
	    }
	    public int getMinutosDif(){
	        return fin.diferencia(inicio);
	    }
};
