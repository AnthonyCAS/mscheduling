
import java.lang.Comparable;

public class tasks implements Comparable<tasks>{

    private Tiempo dates;
    private int type_task; // si es inicio o fin de un bloque

    public tasks(Tiempo time, int t){
        this.type_task = t;
        this.dates = time;
    }
// se ordena por la duracion en minutos en todas las tareas
    public int compareTo(tasks e){
        int temp = this.dates.compareTo(e.dates);
        return (temp == 0)? this.type_task - e.type_task : temp;
    }

    public int getType_task(){
        return this.type_task;
    }
    public Tiempo getTime(){
        return this.dates;
    }
    //para debugear
    public String toString(){
        String aux = new String();
        aux += " " + dates.minutos + " type: " + type_task;

        return aux;
    }
}