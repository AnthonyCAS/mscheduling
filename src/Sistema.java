import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Sistema{

    public Sistema(){
    	this.task = 0;
    	tareas = new ArrayList<tasks>();
    }
    
    //añadir personas a la base de datos tasks(horarios)
    public void put(Person person){
        for(TimePiece b:person.getdisponibilidad()){
        	addTask( new tasks(b.getinicio(), 1) );
        	addTask( new tasks(b.getfin(), -1) );
        }
        task ++;
    }
    //leer archivo planos
	public Person read(String filename, String name) throws IOException{
	    Person person = new Person(name);
	    File f = new File(filename);
	    Scanner in = new Scanner(f);
	    while(in.hasNextLine()){
	        String line = in.nextLine();
	        if(line.isEmpty()) 
	        	continue;
	        String[] tokens = line.split(" ");
	        String day = tokens[0];
	        for(int i=1; i<tokens.length; ++i){
	            String[] times = tokens[i].split("-");
	            Tiempo init = new Tiempo(day, times[0]);
	            Tiempo fin = new Tiempo(day, times[1]);
	            person.addTimePiece( new TimePiece(init, fin) );
	        }
	    }
	    return person;
	    
	}
    public void addTask(tasks t){
    	tareas.add(t);
    }
    //funciones que obtiene el cruce de horarios
    public Person calculate(int temp){
    	System.out.println("Tareas sin ordenar");
         for( tasks t : tareas)
         	System.out.println(t);
        Collections.sort(tareas);
        System.out.println("Tareas ordenadas");
        for( tasks t : tareas)
        	System.out.println(t);
        Person aux = new Person("name");
        int delimitador = 0;
        Tiempo start = new Tiempo();
        Tiempo last = new Tiempo();
        for(tasks t : tareas){
            if(t.getType_task() == 1){
            	delimitador++;
                if(delimitador == task){
                    start = new Tiempo(t.getTime());
                }
            }
            else{
            	delimitador--;
                if(delimitador == task-1){
                    last = new Tiempo(t.getTime());
                    TimePiece bt = new TimePiece(start, last);
                    //si el tiempo es suficiente, i have to put a timeblock 
                    if(bt.getMinutosDif() >= temp)
                        aux.addTimePiece(bt );
                }
            }
        }
        return aux;
    }
public ArrayList<tasks> tareas;
int task;
}
