
import java.util.ArrayList;

public class Person{ 


    public Person(String nombre){
    	this.name = nombre;
        disponibilidad = new ArrayList<TimePiece>();
    }
    public ArrayList<TimePiece> getdisponibilidad(){
        return this.disponibilidad;
    }
    public void addTimePiece(TimePiece t){
        this.disponibilidad.add(t);
    }
    public String toString(){
        String aux = new String();
        if(disponibilidad.size() == 0)
        	return "vacio";
        String dia = null;
        for(TimePiece times:disponibilidad){
            if(dia==null || !times.getinicio().getDayName().equals(dia)){
                dia = times.getinicio().getDayName();
                aux += "\n" + dia;
            }
            aux += " " + times.getinicio().getMinutos_Dia() + "-" + times.getfin().getMinutos_Dia();
        }
        return aux;
    }
    public String getName(){
    	return name;
    }
    private String name;
    private ArrayList<TimePiece> disponibilidad;
}