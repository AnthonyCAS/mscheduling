import java.io.IOException;
import java.lang.Comparable;

public class Tiempo implements Comparable<Tiempo>{
    
    public Tiempo(){
        this.minutos = 0;
    }
    public Tiempo(Tiempo t){
        this.minutos = t.minutos;
    }
    public Tiempo(String day, String Tiempo) throws IOException{
        int dia = getDia(day);
        if(dia == -1)
        	throw new IOException("Invalid Day format");
        this.minutos = dia*60*24; // minutos por ese dia
        String[] tokens = Tiempo.split(":"); // divido el dia en horas y minutos
        Integer horas = new Integer(tokens[0]);
        Integer min = new Integer(tokens[1]);
        this.minutos += 60*horas + min; //obtengo el total de minutos
    }
    //obtengo el dia segun el array de dias
    private int getDia(String day){
        for(int i=0; i<dias.length; ++i ){
            if(dias[i].equals(day))
                return i;
        }
        return -1; // no se encontro el dia, para lanzar la excepcion
    }
    //ordeno por tiempo, con la interfaz comparables
    public int compareTo(Tiempo t){
        return this.minutos - t.minutos;
    }
    
    public String getDayName(){
        return dias[this.minutos/(24*60)];
    }
    public String getMinutos_Dia(){
        String ret = new String();
        int hora =  (minutos%(24*60))/60;
        int minuto = minutos%(24*60)%60;
        ret +=  ((hora<10)? "0"+hora:hora) + ":" + ((minuto<10)? "0"+minuto:minuto);
        return ret;
    }
    public int diferencia(Tiempo t){
        return this.minutos - t.minutos;
    }

    private final static String[] dias = {"mon", "tue", "wed", "thu", "fri", "sat", "sun"};
    public int minutos;
};